package com.example.ShopCart.Repositories;

import com.example.ShopCart.Models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CartRepository extends JpaRepository<Cart, Long> {
    Cart findBySessionValue(String sessionValue);
}
