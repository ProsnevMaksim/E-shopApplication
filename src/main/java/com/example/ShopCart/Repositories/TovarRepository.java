package com.example.ShopCart.Repositories;

import com.example.ShopCart.Models.Tovar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TovarRepository extends JpaRepository<Tovar, Long> {
}
