package com.example.ShopCart.Repositories;

import com.example.ShopCart.Models.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {
    Users findByUsername(String username);

    Users findByActivationCode(String code);
}
