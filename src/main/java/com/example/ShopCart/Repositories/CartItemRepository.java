package com.example.ShopCart.Repositories;

import com.example.ShopCart.Models.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {
}
